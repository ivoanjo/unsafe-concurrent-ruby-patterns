Thread.new { require './background_required' }

sleep 0.1

f = Foo.new
f.hello
f.bye

## Expected output:
#
# $ ruby background_require.rb
# Hello!
# Traceback (most recent call last):
# background_require.rb:7:in `<main>': undefined method `bye' for #<Foo:0x00005622a9a3cbf0> (NoMethodError)
