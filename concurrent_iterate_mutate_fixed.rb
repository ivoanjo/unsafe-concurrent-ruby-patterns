require 'concurrent'

@hash = Concurrent::Map.new

t1 = Thread.new { 1_000_000.times { |n| @hash[n] = true } }
t2 = Thread.new { @hash.each { } while @hash.size < 1_000_000 }

t1.join rescue nil
t2.join rescue nil

## Expected output:
#
# $ ruby concurrent_iterate_mutate_fixed.rb
#
# (No output, finishes successfully)
