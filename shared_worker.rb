class Worker
  def call(foo, bar)
    @foo = foo; @bar = bar
    some_domain_logic
    @foo + @bar
  end

  def some_domain_logic; sleep(rand) end
end

worker = Worker.new

t1 = Thread.new { puts "Result for w1 is #{worker.call(1, 2)}" }
t2 = Thread.new { puts "Result for w2 is #{worker.call(3, 4)}" }

t1.join; t2.join

## Expected output
# $ ruby shared_worker.rb
# Result for w1 is 3
# Result for w2 is 3
#
# or...
#
# $ ruby shared_worker.rb
# Result for w1 is 7
# Result for w2 is 7
