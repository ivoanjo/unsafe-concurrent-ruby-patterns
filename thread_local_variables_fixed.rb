require 'concurrent'

class PerThreadGateway
  def initialize
    @thread_local = Concurrent::ThreadLocalVar.new
  end

  def client
    @thread_local.value ||= new_client
  end

  def new_client
    puts "Creating a new client!"
    :"client#{rand(1000)}"
  end
end

g1 = PerThreadGateway.new; puts "For gateway 1, got #{g1.client}"
g2 = PerThreadGateway.new; puts "For gateway 2, got #{g2.client}"

## Expected output:
#
# $ ruby thread_local_variables_fixed.rb
# Creating a new client!
# For gateway 1, got client274
# Creating a new client!
# For gateway 2, got client468
