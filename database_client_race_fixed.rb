DATABASE_INIT_MUTEX = Mutex.new

def client
  DATABASE_INIT_MUTEX.synchronize { @database_client ||= new_client }
end

def new_client
  sleep 1 # simulate connection setup time
  puts "Created database client"
  :client
end

5.times.map {
  Thread.new { client }
}.each(&:join)

## Expected output:
#
# $ ruby database_client_race_fixed.rb
# Created database client
