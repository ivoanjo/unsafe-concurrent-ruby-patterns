@hash = {}

t1 = Thread.new { 10_000.times { |n| @hash[n] = true } }
t2 = Thread.new { @hash.each { } while @hash.size < 10_000 }

t1.join rescue nil
t2.join rescue nil

## Expected output:
#
# $ ruby concurrent_iterate_mutate.rb
# #<Thread:0x000056110dd4d0b8@concurrent_iterate_mutate.rb:3 run> terminated with exception (report_on_exception is true):
# Traceback (most recent call last):
#         2: from concurrent_iterate_mutate.rb:3:in `block in <main>'
#         1: from concurrent_iterate_mutate.rb:3:in `times'
# concurrent_iterate_mutate.rb:3:in `block (2 levels) in <main>': can't add a new key into hash during iteration (RuntimeError)
