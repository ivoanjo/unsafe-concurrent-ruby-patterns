class PerThreadGateway
  def client
    Thread.current[:connection] ||= new_client
  end

  def new_client
    puts "Creating a new client!"
    :"client#{rand(1000)}"
  end
end

g1 = PerThreadGateway.new; puts "For gateway 1, got #{g1.client}"
g2 = PerThreadGateway.new; puts "For gateway 2, got #{g2.client}"

## Expected output:
#
# $ ruby thread_local_variables.rb
# Creating a new client!
# For gateway 1, got client964
# For gateway 2, got client964
