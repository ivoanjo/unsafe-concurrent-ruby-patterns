require 'concurrent'

array1 = Array.new(5, {})
array2 = Array.new(5) { {} }

hash1 = Hash.new([])
hash2 = Hash.new { [] }

# or even....

local1 = Concurrent::ThreadLocalVar.new([])
local2 = Concurrent::ThreadLocalVar.new { [] }

array1.first[:hello] = :world
array2.first[:hello] = :world

puts "Array1 (with Reference): #{array1.inspect}"
puts "Array2 (with Block): #{array2.inspect}"

hash1[:hello] << :world
hash1[:foo] << :bar
hash2[:hello] << :world
hash2[:foo] << :bar

puts "Hash1 (with Reference): #{hash1[:hello].inspect}"
puts "Hash2 (with Block): #{hash2[:hello].inspect}"

Thread.new { local1.value << :hello }.join
Thread.new { local1.value << :world }.join
Thread.new { local2.value << :hello }.join
Thread.new { local2.value << :world }.join

puts "Threadlocal1 (with Reference): #{Thread.new { local1.value }.value}"
puts "Threadlocal2 (with Block): #{Thread.new { local2.value}.value}"

## Expected output:
#
# $ ruby default_values_by_reference_vs_block.rb
# Array1 (with Reference): [{:hello=>:world}, {:hello=>:world}, {:hello=>:world}, {:hello=>:world}, {:hello=>:world}]
# Array2 (with Block): [{:hello=>:world}, {}, {}, {}, {}]
# Hash1 (with Reference): [:world, :bar]
# Hash2 (with Block): []
# Threadlocal1 (with Reference): [:hello, :world]
# Threadlocal2 (with Block): []
